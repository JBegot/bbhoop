import * as THREE from '../vendor/three.js-master/build/three.module.js';
import {FBXLoader} from '../vendor/three.js-master/examples/jsm/loaders/FBXLoader.js';

var camera, listener, collisions, scene, renderer, timer, meshFloor, ambientLight, light, fbxLoader, loadingManager, basketball, rays, caster;
var keyboard = {};
var player = {
    height: 1.0,
    speed: 0.0325,
    canShoot: true,
};
var loadingScreen = {
    scene: new THREE.Scene(),
    camera: new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000),
    box: new THREE.Mesh(
        new THREE.BoxGeometry(0.5, 0.5, 0.5),
        new THREE.MeshPhongMaterial({color: 0x4444ff})
    )
};
var LOADED = false;
var shoots = [];
var hit= false;
var objects = [];
var score = 0;

init();
animate();

function init() {
    scene = new THREE.Scene();
    scene.background = new THREE.TextureLoader().load('./textures/wall.jpg');

    camera = new THREE.PerspectiveCamera(90, 1920 / 1080, 0.1, 1000);
    camera.position.set(0, player.height, -5);
    camera.lookAt(new THREE.Vector3(0, player.height, 0));

    loadingScreen.box.position.set(0, 0, 5);
    loadingScreen.camera.lookAt(loadingScreen.box.position);
    loadingScreen.scene.add(loadingScreen.box);
    loadingManager = new THREE.LoadingManager();
    loadingManager.onLoad = function () {
        LOADED = true;
    };

    const groundTexture = new THREE.TextureLoader().load('./textures/ground.jpg');
    meshFloor = new THREE.Mesh(
        new THREE.PlaneGeometry(10, 100, 10, 10),
        new THREE.MeshPhongMaterial({map: groundTexture})
    );
    meshFloor.rotation.x -= Math.PI / 2;
    meshFloor.receiveShadow = true;
    scene.add(meshFloor);

    ambientLight = new THREE.AmbientLight(0xffffff, 0.8);
    scene.add(ambientLight);

    light = new THREE.DirectionalLight(0x0022fa, 25);
    light.shadowDarkness = 0.5;
    light.castShadow = true;
    scene.add(light);

    listener = new THREE.AudioListener();
    camera.add( listener );

    const sound = new THREE.Audio(listener);
    const audioLoader = new THREE.AudioLoader();
    audioLoader.load( './sounds/ambient.mp3', function( buffer ) {
        sound.setBuffer( buffer );
        sound.setLoop( true );
        sound.setVolume( 0.1 );
        sound.play();
    });

    fbxLoader = new FBXLoader(loadingManager);

    fbxLoader.load('./models/basketball.FBX', (object) => {
        object.scale.x = object.scale.y = object.scale.z = 0.125;
        object.traverse((child) => {
            if (child.isMesh) {
                child.receiveShadow = true;
                objects.push(child);
            }
        });
        scene.add(object);
        basketball = object;
    });

    const targetTexture = new THREE.TextureLoader().load('./textures/target.png');
    const geometry = new THREE.BoxGeometry(1, 1, 1);
    const material = new THREE.MeshBasicMaterial({map: targetTexture});
    const target = new THREE.Mesh(geometry, material);

    target.castShadow = true;
    target.receiveShadow = false;
    target.position.set(1,1,3);
    objects.push(target);
    scene.add( target );

    light.target = target;

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    document.body.appendChild(renderer.domElement);

    rays = [
        new THREE.Vector3(0, 0, 0.1),
    ];
    caster = new THREE.Raycaster();

    animate();
}

function animate() {

    if (LOADED === false) {
        requestAnimationFrame(animate);
        loadingScreen.box.position.x -= 0.05;
        loadingScreen.box.position.y = Math.sin(loadingScreen.box.position.x);
        renderer.render(loadingScreen.scene, loadingScreen.camera);
        return;
    }

    requestAnimationFrame(animate);

    const time = Date.now() * 0.002;

    for (let index = 0; index < shoots.length; index += 1) {
        if (shoots[index] === undefined) continue;
        if (shoots[index].alive === false) {
            shoots.splice(index, 1);
            continue;
        }
        shoots[index].position.add(shoots[index].velocity);
    }

    //Q key
    if (keyboard[81] && camera.position.x <= 5) {
        camera.position.x += Math.sin(camera.rotation.y + Math.PI / 2) * player.speed;
        camera.position.z += -Math.cos(camera.rotation.y + Math.PI / 2) * player.speed;
    }

    //D key
    if (keyboard[68] && camera.position.x >= -5) {
        camera.position.x += Math.sin(camera.rotation.y - Math.PI / 2) * player.speed;
        camera.position.z += -Math.cos(camera.rotation.y - Math.PI / 2) * player.speed;
    }

    //Space key
    if (keyboard[32] && player.canShoot === true) {

        const sound = new THREE.Audio(listener);
        const audioLoader = new THREE.AudioLoader();
        audioLoader.load( './sounds/throw.wav', function( buffer ) {
            sound.setBuffer( buffer );
            sound.setVolume( 0.5 );
            sound.play();
        });

        player.canShoot = false;
        hit = false;

        const shoot = basketball.clone();
        shoot.alive = true;

        shoot.position.set(
            basketball.position.x,
            basketball.position.y,
            basketball.position.z,
        );
        shoot.velocity = new THREE.Vector3(
            -Math.sin(camera.rotation.y),
            0,
            Math.cos(camera.rotation.y) * 0.2
        );

        shoots.push(shoot);
        scene.add(shoot);

        setTimeout(
            function () {
                shoot.alive = false;
                scene.remove(shoot);
                player.canShoot = true;
                const side = Math.random();
                if (side > 0.5){
                    objects[0].position.set(Math.random() * 5,1,Math.random() * 45);
                }else {
                    objects[0].position.set(Math.random() * -5,1,Math.random() * 45);
                }

                let sound = new THREE.Audio(listener);
                const audioLoader = new THREE.AudioLoader();
                audioLoader.load( './sounds/tp.mp3', function( buffer ) {
                    sound.setBuffer(buffer);
                    sound.setVolume(0.5);
                    sound.play();
                });

                if (hit === false) {
                    score -= 5;
                    document.getElementById("scores").innerHTML = score;
                }
            }, 3000
        );
    }

    basketball.position.set(
        camera.position.x - Math.sin(camera.rotation.y) * 0.4,
        camera.position.y - 0.25 + Math.sin(time + camera.position.x + camera.position.z) * 0.025,
        camera.position.z + Math.cos(camera.rotation.y) * 0.4
    );

    renderer.render(scene, camera);

    collision();
}


function collision()
{
    if (shoots.length > 0 ){

        for (let i = 0; i < rays.length; i += 1) {
            caster.set(shoots[0].position, rays[i]);
            collisions = caster.intersectObjects(objects);

            if (collisions.length > 0) {
                hit = true;
                score++;
                document.getElementById("scores").innerHTML = score;
                let sound = new THREE.Audio(listener);
                const audioLoader = new THREE.AudioLoader();
                audioLoader.load( './sounds/hit.wav', function( buffer ) {
                    sound.setBuffer( buffer );
                    sound.setVolume( 0.5 );
                    sound.play();
                });
            }
        }
    }
}

function keyDown(event) {
    keyboard[event.keyCode] = true;
}

function keyUp(event) {
    keyboard[event.keyCode] = false;
}

window.addEventListener('keydown', keyDown);
window.addEventListener('keyup', keyUp);